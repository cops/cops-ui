#COps UI
IHM Console Operator

## Development

### Install NodeJS with npm
https://nodejs.org/fr/
https://www.npmjs.com/get-npm

### Install the Angular CLI
https://angular.io/guide/setup-local#install-the-angular-cli

### Retrieve and install project

```bash
git clone https://gitlab.ifremer.fr/cops/cops-ui.git
npm install
```
### Run the project

```bash
ng serve
```

Vous pouvez ensuite accéder à l'application :
- <http://127.0.0.1:4200/>


### Run the project with its backend 
configure web service api url in `app.module.ts`