import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Monitored_items_listComponent } from './monitored_items_list.component';

describe('ReportsComponent', () => {
  let component: Monitored_items_listComponent;
  let fixture: ComponentFixture<Monitored_items_listComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Monitored_items_listComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Monitored_items_listComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
