import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {MonitoredItem} from './monitored_item.model';
import {Monitored_itemsService} from './monitored_items.service';
import {BehaviorSubject, of} from 'rxjs';
import {catchError, finalize, map} from 'rxjs/operators';
import {Observable} from 'rxjs'

export class MonitoredItemsDataSource implements DataSource<MonitoredItem> {

    private monitoredItemsSubject = new BehaviorSubject<MonitoredItem[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    public displayedData : any[] = [];

    public totalCount : number = 0;

    public startCursor : string;
    public endCursor : string;


    constructor(private monitoredItemsService: Monitored_itemsService) {}

    connect(collectionViewer: CollectionViewer): Observable<MonitoredItem[] | ReadonlyArray<MonitoredItem>> {
        return this.monitoredItemsSubject;
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.monitoredItemsSubject.complete()
        this.loadingSubject.complete();
    }

    loadMonitoredItems(sort: string = "", pageSize: number = 10, after: string = null, before: string = null) {
        this.loadingSubject.next(true);
        this.monitoredItemsService.searchMessages(sort, pageSize, after, before).pipe(
            catchError(() => of([])),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe(monitoredItems => {
            this.monitoredItemsSubject.next(monitoredItems.data.monitoredItems.edges);
            this.displayedData = monitoredItems.data.monitoredItems.edges
            this.totalCount = monitoredItems.data.monitoredItems.totalCount
            this.startCursor = monitoredItems.data.monitoredItems.pageInfo.startCursor
            this.endCursor = monitoredItems.data.monitoredItems.pageInfo.endCursor
        });
    }

}
