import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MonitoredItem} from '../monitored_item.model';

@Component({
    selector: 'add-tickets-dialog',
    templateUrl: 'add-tickets-dialog.html',
})

export class addTicketsDialog {

    public tickets_to_add : string

    constructor(@Inject(MAT_DIALOG_DATA) public params) {
        this.tickets_to_add = ""
    }
}