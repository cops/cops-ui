import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MonitoredItem} from '../monitored_item.model';

@Component({
    selector: 'delete-tickets-dialog',
    templateUrl: 'delete-tickets-dialog.html',
})

export class deleteTicketsDialog {

    public tickets : string[]
    public tickets_to_delete: string[];

    constructor(@Inject(MAT_DIALOG_DATA) public params) {
        this.tickets = params == null ? [] : params;
        this.tickets_to_delete = []
    }
}