import { TestBed } from '@angular/core/testing';

import { Monitored_itemsService } from './monitored_items.service';

describe('ReportService', () => {
  let service: Monitored_itemsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Monitored_itemsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
