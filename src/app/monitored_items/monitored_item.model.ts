export interface MonitoredItem {
  id: string;
  type: string;
  logicalPath: string;
  logicalGroup: string;
  statusFk: string;
  creationDate: Date;
  updateDate: Date;
  tickets: Array<string>;
}
