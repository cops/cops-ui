import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {MonitoredItem} from './monitored_item.model';
import {Apollo, gql} from 'apollo-angular';

@Injectable({
    providedIn: 'root'
})
export class Monitored_itemsService {

    // Observable string sources
    private monitoredItemDetailsSource = new Subject<MonitoredItem>();
    // Observable string streams
    monitoredItemDetails$ = this.monitoredItemDetailsSource.asObservable();
    constructor(private apollo: Apollo) { }

    searchMessages(sort: string, pageSize: number, after: string = null, before: string): Observable<any>{
        let pageCursorString = ""
        if(after !== null){
            pageCursorString = "first: $pageSize, after:\""+ after +"\""
        }
        else if(before !== null){
            pageCursorString = "last: $pageSize, before:\"" + before + "\""
        }
        else{
            pageCursorString = "first: $pageSize"
        }
        const SELECT_ALL_MONITORED_ITEMS = "" +
            "query allMonitoredItems($pageSize: Int!){" +
                "monitoredItems(sort: ["+ sort +"], "+ pageCursorString +") {" +
                    "pageInfo {" +
                        "startCursor endCursor hasNextPage hasPreviousPage" +
                    "} " +
                    "totalCount " +
                    "edges {" +
                        "node {" +
                            "id type logicalPath statusFk creationDate updateDate " +
                            "lastEventDate logicalGroup tickets labels data" +
                        "}" +
                    "}" +
                "}" +
            "}"

        return this.apollo.query({
            query: gql(SELECT_ALL_MONITORED_ITEMS),
            variables: {
                pageSize:pageSize
            },
            errorPolicy: 'all'
        });
    }

    addTicketsToMonitoredItem(monitoredItemId, tickets: string[]) {
        const ADD_TICKETS_TO_MONITORED_ITEM_POST = gql`
          mutation updateMonitoredItem($id: String!, $tickets: [String!]) {
            updateMonitoredItem (id: $id, tickets: $tickets) 
            {
              monitoredItem {
                id
                tickets
              }
            }
          }
        `;

        return this.apollo.mutate({
            mutation: ADD_TICKETS_TO_MONITORED_ITEM_POST,
            variables: {
                id: monitoredItemId,
                tickets: tickets
            },
            errorPolicy: 'all'
        });
    }

    deleteTicketsFromMonitoredItem(monitoredItemId, tickets: string[]) {
        const DELETE_TICKETS_FROM_MONITORED_ITEM_POST = gql`
          mutation removeTicketsFromMonitoredItem($id: String!, $tickets: [String!]) {
            removeTicketsFromMonitoredItem (id: $id, tickets: $tickets) 
            {
              monitoredItem {
                id
                type
                logicalPath
                statusFk
                creationDate
                updateDate
                lastEventDate
                logicalGroup
                tickets
                labels
                data
              }
            }
          }
        `;
        return this.apollo.mutate({
            mutation: DELETE_TICKETS_FROM_MONITORED_ITEM_POST,
            variables: {
                id: monitoredItemId,
                tickets: tickets
            }
        });
    }

    getMonitoredItemData(monitored_item_id) {
        this.apollo
            .query({
                query: gql`
                query monitoredItemById($monitored_item_id: String!)
                {
                    monitoredItems(filters: {id: $monitored_item_id}){
                        edges {
                            node {
                                id
                                type
                                logicalPath
                                statusFk
                                creationDate
                                updateDate
                                lastEventDate
                                logicalGroup
                                tickets
                                labels
                                data
                            } 
                        }
                    }
                }
                `,
                variables:{monitored_item_id:monitored_item_id},
            })
            .subscribe((result: any) => {
                let mi = result?.data?.monitoredItems.edges[0].node
                this.monitoredItemDetailsSource.next(mi as MonitoredItem)
            })
        ;
    }

    unlockMonitoredItems(monitoredItemIdList: string[]){
        const UNLOCK_MONITORED_ITEMS = gql`
          mutation unlock($monitoredItemIdList: [String!]) {
            unlock (monitoredItemIdList: $monitoredItemIdList) 
            {
                unlockedFilesSuccess,
                unlockedFilesFailure
            }
          }
        `;
        return this.apollo.mutate({
            mutation: UNLOCK_MONITORED_ITEMS,
            variables: {
                monitoredItemIdList: monitoredItemIdList
            }
        });
    }
}
