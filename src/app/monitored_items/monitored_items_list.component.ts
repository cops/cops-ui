import {Component, OnInit, ViewChild} from '@angular/core';
import {Monitored_itemsService } from './monitored_items.service';
import {MonitoredItem} from './monitored_item.model';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {Subscription} from 'rxjs/Subscription';
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {SelectionModel} from '@angular/cdk/collections';
import {MonitoredItemsDataSource} from './monitored-items-data-source';
import {addTicketsDialog} from './monitored-items-dialogs/add-tickets-dialog.component';
import { deleteTicketsDialog } from './monitored-items-dialogs/delete-tickets-dialog.component';
import {map, tap} from 'rxjs/operators';
import {merge} from 'rxjs';


@Component({
  selector: 'monitored-item-list',
  templateUrl: './monitored_items_list.component.html',
  styleUrls: ['./monitored_items_list.component.css']
})
export class Monitored_items_listComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public monitored_itemsService: Monitored_itemsService;
  messageType = '1';
  public sort_gql : string = "UPDATE_DATE_DESC";
  public page_size : number = 20;
  private pageIndex : number = 0;

  visibility_unlock_button = "hidden";

  dataSource: MonitoredItemsDataSource;
  displayedColumns: string[] = ['select','TYPE','LOGICAL_PATH','LOGICAL_GROUP','STATUS_FK',
    'CREATION_DATE', 'UPDATE_DATE','TICKETS','actions'];
  selection = new SelectionModel<MonitoredItem>(true, []);

  constructor(private router: Router, public monitoreditemsService: Monitored_itemsService,public dialog: MatDialog) {
    this.monitored_itemsService = monitoreditemsService;
  }

  isRowSelectable(row):boolean{
    return row.type == 'processrunner' && row.statusFk == 'ERROR'
  }

  ngOnInit(): void {
    this.dataSource = new MonitoredItemsDataSource(this.monitoreditemsService);
    this.dataSource.loadMonitoredItems(this.sort_gql,this.page_size);

  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
      this.sort_gql = this.sort.active+'_'+this.sort.direction.toUpperCase()
    });

    merge(this.sort.sortChange, this.paginator.page)
        .pipe(
            tap(() => this.loadMonitoredItemsPage())
        )
        .subscribe();
  }

  loadMonitoredItemsPage() {
    if(this.paginator.pageIndex == 0){
      this.dataSource.loadMonitoredItems(
          this.sort_gql,
          this.paginator.pageSize,
      );
    }
    else if(this.paginator.pageIndex > this.pageIndex){
      this.dataSource.loadMonitoredItems(
          this.sort_gql,
          this.paginator.pageSize,
          this.dataSource.endCursor
      );
    }
    else{
      this.dataSource.loadMonitoredItems(
          this.sort_gql,
          this.paginator.pageSize,
          null,
          this.dataSource.startCursor
      );
    }
    this.pageIndex = this.paginator.pageIndex
  }

  /** Whether the number of selected elements matches the total number of selectable rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.displayedData
        .filter(
            (row) => {
              return this.isRowSelectable(row.node)
            }).length;
    return numSelected === numRows;
  }

  /** Selects all selectable rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if(this.isAllSelected()){
      this.selection.clear()
    }
    else {
      this.dataSource.displayedData.filter((row)=>{
        return this.isRowSelectable(row.node)
      }).forEach(row => this.selection.select(row.node))
    }
    this.updateUnlockButtonVisibility()
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: MonitoredItem): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  updateUnlockButtonVisibility(): void {
    if(this.selection.selected.length > 0){
      this.visibility_unlock_button = "visible"
    }
    else{
      this.visibility_unlock_button = "hidden"
    }
  }

  goToLink(url: any) {
    window.open(url, "_blank");
  }

  goToMonitoredItemDetails(monitoredItemId: any) {
    this.router.navigate(['cops/monitored_item', monitoredItemId]);
  }

  toggleMonitoredItem(row) {
    this.selection.toggle(row)
    this.updateUnlockButtonVisibility()
  }

  addTicketsDialog(row): void {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(addTicketsDialog,dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.addTickets(row.id, result);
        this.dataSource.loadMonitoredItems(this.sort_gql,this.page_size);
      }
    });
  }

  deleteTicketsDialog(row): void {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.data = row.tickets

    const dialogRef = this.dialog.open(deleteTicketsDialog,dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if(result?.length > 0){
        this.removeTickets(row.id, result)
        this.dataSource.loadMonitoredItems(this.sort_gql,this.page_size);
      }
    });
  }

  addTickets(monitoredItemId, ticketsToAdd) {
    let ticketsToAddSplit = ticketsToAdd?.split(",");
    this.monitoreditemsService.addTicketsToMonitoredItem(
        monitoredItemId,ticketsToAddSplit
    ).subscribe();
  }

  removeTickets(monitoredItemId,ticketsToDelete) {
    this.monitoreditemsService.deleteTicketsFromMonitoredItem(
        monitoredItemId,ticketsToDelete
    ).subscribe(data => {
      console.log(data)
      this.dataSource.loadMonitoredItems(this.sort_gql,this.paginator.pageSize);
    });
  }

  unlock_Selected_Files() {
    let monitoredItemsList : string[] = []
    this.selection.selected.forEach((mi)=>{
      monitoredItemsList.push(mi.id)
    })

    console.log(monitoredItemsList)

    this.monitoreditemsService.unlockMonitoredItems(monitoredItemsList)
        .subscribe((data)=>{
          // console.log("result data:\n",data)
          this.dataSource.loadMonitoredItems(this.sort_gql,this.page_size)
          this.selection.clear()
        });

    // const dialogConfig = new MatDialogConfig();
    //
    // dialogConfig.autoFocus = true;
    //
    // const dialogRef = this.dialog.open(`<div>"ici un component pour le message de succes ou erreur"</div>`,dialogConfig);

  }
}
