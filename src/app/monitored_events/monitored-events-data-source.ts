import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {MonitoredEvent} from './monitored_event.model';
import {BehaviorSubject, Observable, of} from 'rxjs'
import {Monitored_eventsService} from './monitored_events.service';
import {catchError, finalize} from 'rxjs/operators';

export class MonitoredEventsDataSource implements DataSource<MonitoredEvent> {

    private monitoredEventsSubject = new BehaviorSubject<MonitoredEvent[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    public displayedData : any[] = [];

    public totalCount : number = 0;

    public startCursor : string;
    public endCursor : string;

    public firstDisplayedRow : any


    constructor(private monitoredEventsService: Monitored_eventsService) {}

    connect(collectionViewer: CollectionViewer): Observable<MonitoredEvent[] | ReadonlyArray<MonitoredEvent>> {
        return this.monitoredEventsSubject;
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.monitoredEventsSubject.complete()
        this.loadingSubject.complete();
    }

    loadEvents(monitored_item_id: string = "", sort: string = "", pageSize: number = 10, after: string = null, before: string = null) {
        this.loadingSubject.next(true);

        this.monitoredEventsService.searchEvents(monitored_item_id, sort, pageSize, after, before).pipe(
            catchError(() => of([])),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe(monitoredEvents => {
            this.monitoredEventsSubject.next(monitoredEvents.data.events.edges);
            this.displayedData = monitoredEvents.data.events.edges
            this.totalCount = monitoredEvents.data.events.totalCount
            this.startCursor = monitoredEvents.data.events.pageInfo.startCursor
            this.endCursor = monitoredEvents.data.events.pageInfo.endCursor
            this.firstDisplayedRow = monitoredEvents.data.events.edges[0]
        });
    }

}
