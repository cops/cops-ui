import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Monitored_Item_Details_Component } from './monitored_item_details.component';

describe('ReportsComponent', () => {
  let component: Monitored_Item_Details_Component;
  let fixture: ComponentFixture<Monitored_Item_Details_Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Monitored_Item_Details_Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Monitored_Item_Details_Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
