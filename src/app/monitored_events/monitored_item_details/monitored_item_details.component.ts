import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {AgGridAngular} from 'ag-grid-angular';

import {MatDialog} from '@angular/material/dialog';
import {Subscription} from 'rxjs/Subscription';
import {MonitoredItem} from '../../monitored_items/monitored_item.model';
import {Monitored_itemsService} from '../../monitored_items/monitored_items.service';

@Component({
  selector: 'monitored-item-details',
  templateUrl: './monitored_item_details.component.html',
  styleUrls: ['./monitored_item_details.component.css']
})
export class Monitored_Item_Details_Component implements OnInit {
  monitored_itemService: Monitored_itemsService
  monitoredItemDetailsSubscription: Subscription;

  @Input() monitored_item_id: string
  monitored_item: MonitoredItem

  showFiller = false;
  constructor(public monitoredItemsService: Monitored_itemsService) {
    this.monitored_itemService = monitoredItemsService;
  }

  ngOnInit(): void {
    this.monitoredItemDetailsSubscription = this.monitored_itemService.monitoredItemDetails$.subscribe(
        monitored_item => {
          this.monitored_item = monitored_item
        }
    );
    this.monitored_itemService.getMonitoredItemData(this.monitored_item_id);
  }

  ngOnDestroy() {
    this.monitoredItemDetailsSubscription.unsubscribe();
  }
}
