export class MonitoredEvent {
  id: string;
  creationDate: Date;
  eventDate: Date;
  levelFk: string;
  subject: string;
  message: string;
  monitoredItemFk: string;
  logFile: string;

  constructor(id, creationDate, eventDate, levelFk, subject, message, monitoredItemFk,logFile) {
    this.id = id;
    this.creationDate = creationDate;
    this.eventDate = eventDate;
    this.levelFk = levelFk;
    this.subject = subject;
    this.message = message;
    this.monitoredItemFk = monitoredItemFk;
    this.logFile = logFile;
  }
}
