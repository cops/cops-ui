import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Monitored_events_listComponent } from './monitored_events_list.component';

describe('ReportsComponent', () => {
  let component: Monitored_events_listComponent;
  let fixture: ComponentFixture<Monitored_events_listComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Monitored_events_listComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Monitored_events_listComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
