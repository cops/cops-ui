import {Component, OnInit, ViewChild} from '@angular/core';
import {Monitored_eventsService} from './monitored_events.service';
import {MonitoredEvent} from './monitored_event.model';
import {ActivatedRoute, Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {SelectionModel} from '@angular/cdk/collections';
import {MonitoredEventsDataSource} from './monitored-events-data-source';
import {merge, of} from 'rxjs';
import {catchError, finalize, tap} from 'rxjs/operators';
import {CdkTextareaAutosize} from '@angular/cdk/text-field';

@Component({
  selector: 'monitored-events-list',
  templateUrl: './monitored_events_list.component.html',
  styleUrls: ['./monitored_events_list.component.css'],
  providers: [Monitored_eventsService]
})
export class Monitored_events_listComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('autosize') autosize: CdkTextareaAutosize;

  public monitored_eventsService: Monitored_eventsService;
  public sort_gql : string = "EVENT_DATE_DESC";
  public page_size : number = 10;
  private pageIndex : number = 0;

  dataSource: MonitoredEventsDataSource;
  displayedColumns: string[] = ['EVENT_DATE','LEVEL_FK','SUBJECT'];
  selection = new SelectionModel<MonitoredEvent>(false, []);

  sub: any;
  mi_id: string

  public selectedEventMessage: any;
  public logContent: any;
  selectedEvent:MonitoredEvent
  loadingLogContent: boolean;

  constructor(private route: ActivatedRoute, public monitoredeventsService: Monitored_eventsService) {
    this.monitored_eventsService = monitoredeventsService;
  }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.mi_id = params.monitored_item_id;
    });
    this.dataSource = new MonitoredEventsDataSource(this.monitored_eventsService);
    this.dataSource.loadEvents(this.mi_id, this.sort_gql,this.page_size);
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
      this.sort_gql = this.sort.active+'_'+this.sort.direction.toUpperCase()
    });

    merge(this.sort.sortChange, this.paginator.page)
        .pipe(
            tap(() => this.loadEventsPage())
        )
        .subscribe();
  }

  loadEventsPage() {
    if(this.paginator.pageIndex == 0){
      this.dataSource.loadEvents(
          this.mi_id,
          this.sort_gql,
          this.paginator.pageSize,
      );
    }
    else if(this.paginator.pageIndex > this.pageIndex){
      this.dataSource.loadEvents(
          this.mi_id,
          this.sort_gql,
          this.paginator.pageSize,
          this.dataSource.endCursor
      );
    }
    else{
      this.dataSource.loadEvents(
          this.mi_id,
          this.sort_gql,
          this.paginator.pageSize,
          null,
          this.dataSource.startCursor
      );
    }
    this.pageIndex = this.paginator.pageIndex
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: MonitoredEvent): string {
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  toggleEvent(row) {
    if(this.selection.selected[0]?.id !== row.id){
      this.selectedEvent = row;
      this.loadLogContent(row.id)
    }
    else{
      this.selectedEvent = this.logContent = null
      this.loadingLogContent = false
    }

    this.selection.toggle(row)

    var selectedRows = this.selection.selected;
    this.selectedEventMessage = selectedRows.length === 1 ? JSON.parse(selectedRows[0].message) : '';
  }

  loadLogContent(monitored_item_id: string = ""){
    this.loadingLogContent = true;

    this.monitored_eventsService.getLogFileContent(monitored_item_id).pipe(
        catchError((error,caught) => of()),
        finalize(() => this.loadingLogContent = false)
    ).subscribe(data => {
      this.logContent = data?.data?.logFile
    });
  }
}
