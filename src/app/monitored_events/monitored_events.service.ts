import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Apollo, gql} from 'apollo-angular';

@Injectable({
    providedIn: 'root'
})
export class Monitored_eventsService {
    constructor(private apollo: Apollo) { }

    searchEvents(monitored_item_id: string, sort: string, pageSize: number, after: string = null, before: string): Observable<any>{
        let pageCursorString = ""
        if(after !== null){
            pageCursorString = "first: $pageSize, after:\""+ after +"\""
        }
        else if(before !== null){
            pageCursorString = "last: $pageSize, before:\"" + before + "\""
        }
        else{
            pageCursorString = "first: $pageSize"
        }
        const SELECT_EVENTS_BY_MONITORED_ITEM_ID = "" +
            "query eventsByMonitoredItemId($monitored_item_id: String!, $pageSize: Int!)"+
            "{"+
            "    events(filters: {monitoredItemFk: $monitored_item_id}, sort: ["+ sort +"], "+ pageCursorString +"){"+
            "       pageInfo {"+
            "           startCursor"+
            "           endCursor"+
            "           hasNextPage"+
            "           hasPreviousPage"+
            "       }"+
            "       totalCount"+
            "       edgeCount"+
            "        edges {"+
            "            cursor"+
            "            node {"+
            "                id"+
            "                eventDate"+
            "                levelFk"+
            "                subject"+
            "                message"+
            "                logFile"+
            "            }"+
            "        }"+
            "   }"+
            "}";
        return this.apollo.query({
            query: gql(SELECT_EVENTS_BY_MONITORED_ITEM_ID),
            variables: {
                monitored_item_id:monitored_item_id,
                pageSize:pageSize
            },
            errorPolicy: 'all'
        });
    }

    getLogFileContent(event_id: string): Observable<any>{
        const GET_LOG_FILE_CONTENT = "" +
            "query logFile($event_id: String!)"+
            "{"+
            "    logFile(q:$event_id)" +
            "}";

        return this.apollo.query({
            query: gql(GET_LOG_FILE_CONTENT),
            variables: {
                event_id:event_id
            },
            errorPolicy: 'all'
        });
    }
}
