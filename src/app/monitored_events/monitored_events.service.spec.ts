import { TestBed } from '@angular/core/testing';

import { Monitored_eventsService } from './monitored_events.service';

describe('ReportService', () => {
  let service: Monitored_eventsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Monitored_eventsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
