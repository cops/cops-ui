### STAGE 1: Build ###
FROM node:12.7-alpine AS build

WORKDIR /usr/src/app

COPY package.json package-lock.json ./

RUN npm install

COPY . .

RUN npm run build:prod

RUN rm -R /usr/src/app/node_modules

### STAGE 2: Run ###
FROM nginx:1.17.1-alpine

RUN mkdir /usr/share/nginx/html/cops-ui

COPY --from=build /usr/src/app/dist/* /usr/share/nginx/html/cops-ui/
